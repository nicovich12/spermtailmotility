
function ParsedData = ZVIHashtable2Structure(input)
% ZVIHashtable2Structure.m
% No guarantee this gets every field correctly. 
% Specifically written for CreanFFTviaZVI.m.

% Output metaData.raw to structure

% From loci.common.DateTools.java
ZVIEpoch = 2921084975759000; 

if isfield(input, 'raw')
    metaData = input.raw;
else
    metaData = input;
end

clear input;

import java.util.*;

%create iterator;
keys = ArrayList(metaData.keySet());

iterator = keys.iterator();
Nkeys = keys.size();

keyHold = cell(Nkeys, 1);

m = 0;

%iterate over all keys
while iterator.hasNext()

    key=iterator.next();
%     val=metaData.get(key);

    keyHold{m+1} = key;

    m = m+1;
end

maxKeyLength = max(cell2mat(cellfun(@numel, keyHold, 'UniformOutput', false)));
keyPad = cell(maxKeyLength, 1);
for k = 1:Nkeys
    
    keyPad{k, 1} = strcat(keyHold{k}, sprintf('%*s', (maxKeyLength - numel(keyHold{k}) - 1), ' ') );
    
end

keyPad = sortcell(keyPad, 1);

% Start assigning into structure.
N_Positions = str2double(metaData.get('Image Count T'));
if isempty(N_Positions) || isnan(N_Positions)
    N_Positions = str2double(metaData.get('NumberOfRawImages'));
end

Data.XPost = zeros(N_Positions, 1);
Data.YPost = zeros(N_Positions, 1);
Data.Timestamp = zeros(N_Positions, 1);
Data.ImageChannelIndex = zeros(N_Positions, 1);

keys = ArrayList(metaData.keySet());
iterator = keys.iterator();
% Nkeys = keys.size();

m = 0;
InfoHold = [];


while iterator.hasNext()
    
    key=iterator.next();
    val=metaData.get(key);
    
    keySpot = strfind(key, ' ');
    key1 = strrep(key, ' ', '');
    trunk = strfind(key1, '#');
    
    if ~isempty(trunk)
        
        fieldname = key1(1:(trunk-1));
        numFind = strfind(key1, '#');
        numHere = str2double(key1((numFind+1):end));
        
    elseif isempty(trunk) && ~isempty(keySpot)
                
        fieldname = strtok(key, ' ');
        numHere = str2double(key((keySpot(end)+1):end)); % If there's no # in name, then numHere is the numeral coming after the last space in the key 
    else
        
        fieldname = strtok(key, ' ');

    end
    
    keyFound = 0;

    
    if strfind(key, 'X position for position') == 1
        
        Data.XPost(numHere) = str2double(val);
        keyFound = 1;

    elseif strfind(key, 'Y position') == 1
        
        Data.YPost(numHere) = str2double(val);
        keyFound = 1;

    elseif strfind(key, 'Timestamp') == 1
        
        Data.Timestamp(numHere) = str2double(val);
        keyFound = 1;
        
    elseif strfind(key, 'Image Channel Index') == 1
        
        try 
            Data.ImageChannelIndex(numHere) = str2double(val);
        catch
            Data.(['ImageChannelIndex' num2str(numHere)]) = (val);
        end
        
        keyFound = 1;
        
    elseif strfind(key, 'Camera') == 1
        
        RestOfKeyStart = strfind(key1, 'a');
        RestOfKey = key1(RestOfKeyStart(2)+1:end);
        
        if strcmp(RestOfKey, '0') == 1
            RestOfKey = 'Camera0';
        elseif isempty(RestOfKey)
            RestOfKey = 'Camera';
        end
        
        Data.Camera.(RestOfKey) = val;
        keyFound = 1;
        
	elseif strfind(key, 'Acquisition') == 1
        
        RestOfKeyStart = strfind(key1, 'n');
        RestOfKey = key1(RestOfKeyStart(1)+1:end);

        Data.Acquisition.(RestOfKey) = val;
        keyFound = 1;       
        
	elseif strfind(key, 'AxioCam') == 1
        
        RestOfKeyStart = strfind(key1, 'm');
        RestOfKey = key1(RestOfKeyStart(1)+1:end);

        Data.AxioCam.(RestOfKey) = val;
        keyFound = 1;   
        
	elseif strfind(key, 'Image') == 1
        
        RestOfKeyStart = strfind(key1, 'e');
        RestOfKey = key1(RestOfKeyStart(1)+1:end);
        
        RestOfKey(regexp(RestOfKey, '[()]')) = '';

        Data.Image.(RestOfKey) = val;
        keyFound = 1;
        
	elseif strfind(key, 'Microscope') == 1
        
        RestOfKeyStart = strfind(key1, 'e');
        RestOfKey = key1(RestOfKeyStart(1)+1:end);

        Data.Microscope.(RestOfKey) = val;
        keyFound = 1;  
        
	elseif strfind(key, 'Objective') == 1
        
        RestOfKeyStart = strfind(key1, 'e');
        RestOfKey = key1(RestOfKeyStart(2)+1:end);
        
        RestOfKey(regexp(RestOfKey, '[.]')) = '';

        Data.Objective.(RestOfKey) = val;
        keyFound = 1;        
        
	elseif strfind(key, 'Scale') == 1
        
        RestOfKeyStart = strfind(key1, 'e');
        RestOfKey = key1(RestOfKeyStart(1)+1:end);

        Data.Scale.(RestOfKey) = val;
        keyFound = 1; 
        
	elseif strfind(key, 'Stage') == 1
        
        RestOfKeyStart = strfind(key1, 'e');
        RestOfKey = key1(RestOfKeyStart(1)+1:end);

        Data.Stage.(RestOfKey) = val;
        keyFound = 1; 
        
	elseif strfind(key, 'ImageWidth') == 1

        RestOfKey = 'ImageWidth';

        Data.Image.(RestOfKey) = val;
        keyFound = 1; 
        
    elseif strfind(key, 'tag') == 1
        
        RestOfKey = key;
        RestOfKey(regexp(RestOfKey, '[ ]')) = 'x';
        
        Data.Tag.(RestOfKey) = val;
        keyFound = 1; 
        
    else
        
        RestOfKey = key;
        RestOfKey(regexp(RestOfKey, '[[]]')) = '_';
        RestOfKey(regexp(RestOfKey, '[ ]')) = '';
        
        try
            Data.Experiment.(RestOfKey) = val;
        catch
            keyNameHere = ['NV_' RestOfKey];
            Data.Experiment.(keyNameHere) = val;
        end
        
        keyFound = 1; 
        
     end

    m = m+1;

    if keyFound == 1
        keyPad{strcmp(key, keyPad)} = [];
    end
    
end

keyPad(cellfun(@isempty,  keyPad)) = [];

% Convert timestamps to real time
Dstamp = str2double(Data.Acquisition.Date)/1600 - ZVIEpoch;
Dstamp0 = str2double(Data.Acquisition.Date0)/1600 - ZVIEpoch;
Data.Acquisition.Date0 = char(Date(Dstamp0));
Data.Acquisition.Date = char(Date(Dstamp));


% Create vector for frame timestamp in milliseconds
Data.RelTimestamp = cumsum([0; diff(Data.Timestamp)]/1600);

ParsedData = Data;
