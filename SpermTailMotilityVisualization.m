function SpermTailMotilityVisualization

% Initialize main figure
VisFig = figure();
set(VisFig, 'Position', [100 100 242 545], 'Tag', 'VisFig', 'MenuBar', 'none', ...
    'Toolbar', 'none', 'NumberTitle', 'off', 'DeleteFcn', @CloseVisFig);

ImageFig = figure();
set(ImageFig, 'Position', [380 80 600 600], 'Tag', 'ImageFig', 'MenuBar', 'none', ...
    'Toolbar', 'figure', 'NumberTitle', 'off', 'DeleteFcn', @CloseImageFig);

% Initialize Panels
AxisPanel = uipanel('Parent', ImageFig, 'Units', 'normalized', ...
    'Position', [0 0 1 1], 'Tag', 'AxisPanel');

ButtonPanel = uipanel('Parent', VisFig, 'Units', 'normalized', ...
    'Position', [0 0 1 1], 'Tag', 'ButtonPanel');

% Initialize Axes
logo = Vector2Colormap(BMIFLogoGenerate, 'gray');

ImageAxes = axes('Parent', AxisPanel, 'Units', 'normalized', ...
    'Position', [.02 .02 .96 .96], 'Tag', 'ImageAxes');
HoldImg = image(logo, 'Parent', ImageAxes);

set(ImageAxes, 'XTick', [], 'YTick', []);
axis image;
colormap('jet');

%%%%%%%%%%%%
% Set up toolbar
hToolbar = findall(ImageFig,'tag','FigureToolBar');
AllToolHandles = findall(hToolbar);
ToolBarTags = get(AllToolHandles,'Tag');
ToolsToKeep = {'FigureToolBar'; 'Exploration.DataCursor'; 'Exploration.Pan'; 'Exploration.ZoomOut'; 'Exploration.ZoomIn'};
WhichTools = ~ismember(ToolBarTags, ToolsToKeep);
delete(AllToolHandles(WhichTools));


%%%%%%%%%%%%%%%
% Figure listener to watch for ROI editing to end


% % first you define your function handle to be called (this one here is the enterFunction
%   pointerBehavior.traverseFcn = @MouseoverROI;
% % then you assign it to your graphics object
%   iptSetPointerBehavior(ImageAxes, pointerBehavior.traverseFcn);
% % finally you have to enable the pointer manager for the figure:
%   iptPointerManager(VisFig);



%FigListener = addlistener(ImageFig, 'WindowButtonMotionFcn', 'PostSet', @FigListenerFcn); 


% Initialize Buttons and Text Box

LoadButton = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [.05 .8 .9 .1], 'FontSize', 12, ...
    'String', 'Load File', 'Callback', @LoadPathFcn, 'Tag', 'LoadButton');

LoadText = uicontrol('Parent', ButtonPanel, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [.05 .7 .9 .05], ...
    'String', '', 'Enable', 'off', 'Tag', 'LoadText');

SelectPts = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [.05 .2 .9 .1], 'FontSize', 12,...
    'String', 'Add Region', 'Callback', @SelectPtsFcn, 'Enable', 'off');

PlotPts = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.05 .05 .9 .1], 'FontSize', 12, ...
    'String', 'Export Selection', 'Callback', @PlotPtsFcn, 'Enable', 'off');


Data.handles.VisFig = VisFig;
Data.handles.ImageFig = ImageFig;
Data.handles.AxisPanel = AxisPanel;
Data.handles.ButtonPanel = ButtonPanel;
Data.handles.ImageAxes = ImageAxes;

Data.handles.LoadButton = LoadButton;
Data.handles.LoadText = LoadText;
Data.handles.SelectPts = SelectPts;
Data.handles.PlotPts = PlotPts;

Data.PathName = pwd;

Data.Mask = zeros(size(logo, 1), size(logo, 2));
Data.Images = logo;

Data.AxisStartPosition = get(ImageFig, 'Position');

Data.TextColor = [1 .82 .72];
Data.ROIHandle = [];
Data.ROIobj = [];

guidata(VisFig, Data);

set(ImageFig, 'WindowButtonMotionFcn', @MouseoverROI);
set(ImageFig, 'WindowButtonUpFcn', @RecalcStats);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback functions

%%%%%


%%%%%

    function LoadPathFcn(varargin)
        
        Data = guidata(VisFig);
        Data = guidata(findobj('Tag', 'VisFig'));
        
        [FileName, PathName, FilterIndex] = uigetfile(fullfile(Data.PathName, '*.mat'), 'MALAB Data File (*.mat)');
        
        if  isequal(FileName,0) || isequal(PathName,0)
            % Cancel pressed.  No file selected.
        else
        
            Data.FileContents = load(fullfile(PathName, FileName));

            Data.Images = Data.FileContents.FFTImg;
            Data.PathName = PathName;

            set(Data.handles.LoadText, 'String', fullfile(PathName, FileName), ...
                'Enable', 'on');
            set(Data.handles.SelectPts, 'Enable', 'on');


            % Clear out old ROIs and associated Data
            if ~isempty(Data.ROIobj)
                for k = 1:length(Data.ROIobj)
                    Data.ROIobj(k).delete();
                    
                end
                Data.ROIobj = [];
                Data.ROItext = {};
                Data.ROIHandle = [];
                set(Data.handles.PlotPts, 'Enable', 'off');
            end
            
            guidata(findobj('Tag', 'VisFig'), Data);

            DisplayImageInAxes;
        
        end
        
    end

%%%%%

    function SelectPtsFcn(varargin)
        
        Data = guidata(VisFig);
        
        NewObj = impolylabel(Data.handles.ImageAxes);
        
        NewObj.addNewPositionCallback(@SilenceMouseover);
        NewObj.setColor([.4 1 .2]);
        
        HandleHolder = get(handle(NewObj), 'Children');
        NewObj.setHandle(get(HandleHolder(1), 'Parent'));
        
        fcn = makeConstrainToRectFcn('impoly',get(Data.handles.ImageAxes,'XLim'),...
            get(Data.handles.ImageAxes,'YLim'));
        setPositionConstraintFcn(NewObj,fcn);
        
        %ObjListener = addlistener(NewObj, 'PostGet', @ROIListener);
        
        Data.ROIobj = [Data.ROIobj; NewObj];
        Data.ROIHandle = [Data.ROIHandle; NewObj.Handle];
        
        set(Data.handles.PlotPts, 'Enable', 'on');
        
        guidata(VisFig, Data);
        
        
    end

%%%%%

    function DisplayImageInAxes(varargin)
        
        Data = guidata(findobj('Tag', 'VisFig'));
%         
        % Reshape figure position to keep it nice and tight
        AxPost = Data.AxisStartPosition;
        FigPost = get(ImageFig, 'Position');
        AxPost(1:2) = FigPost(1:2);
        YPixperPix = AxPost(3)/size(Data.Images, 1);
        XPixperPix = AxPost(4)/size(Data.Images, 2);
        
        XorYRescale = XPixperPix == min([XPixperPix YPixperPix]); % 0 for X, 1 for Y
        if XorYRescale
            % Rescale Y axis to fit 
            MoveUp = (AxPost(3) - (AxPost(4)/size(Data.Images, 2))*size(Data.Images, 1))/2;
            ScaleUp = (size(Data.Images, 1)*XPixperPix);
            
            set(Data.handles.ImageFig, 'Position',...
                [AxPost(1) AxPost(2) AxPost(3) ScaleUp]);
           
        else
            % Rescale X axis to fit 
            MoveUp = (AxPost(4) - (AxPost(3)/size(Data.Images, 1))*size(Data.Images, 2))/2;
            ScaleUp = (size(Data.Images, 2)*YPixperPix);
            
            set(Data.handles.ImageFig, 'Position', ...
                [AxPost(1) AxPost(2) ScaleUp AxPost(4)]);
        end
        
        
        
%             Data.PixelsPerPixel = 
        
        image(Vector2Colormap_setscale(Data.Images, 'jet', [0 Data.FileContents.FreqCutoff]),...
            'Parent', Data.handles.ImageAxes);
        set(Data.handles.ImageAxes, 'XTick', [], 'YTick', []);
        axis(Data.handles.ImageAxes, 'image');
        
        Data.Mask = zeros(size(Data.Images, 1), size(Data.Images, 2));

    end

%%%%%



%%%%%

%     function FigListenerFcn(varargin)
%         
%         Data = guidata(VisFig);
%         
%         %set(VisFig, 'WindowButtonUpFcn', @RecalcStats);
%         % disp('Fig listener')
%          
%          fcnNow = get(ImageFig, 'WindowButtonMotionFcn');
%          
%          if ~isempty(fcnNow)
% %             disp(get(VisFig, 'WindowButtonMotionFcn'));
% %             MotionToggle = 'on';
%          else
%               disp('Recalc Mask')
% %              MotionToggle = 'off';
%              % Here's where you want to set mask and stats updates.
%              set(ImageFig, 'WindowButtonMotionFcn', @MouseoverROI);
%          end
% 
%         
%         
%         % Make mouseover function for ROI to display stats of region
%         
%         guidata(VisFig, Data);
%         
%     end

%%%%%

    function MouseoverROI(varargin)
        
        Data = guidata(VisFig);
        
        FcnNow = functions(get(ImageFig, 'WindowButtonMotionFcn'));

        if strcmp(FcnNow.function, 'ROItoExcel/MouseoverROI')
        
            AxPost = getpixelposition(Data.handles.ImageAxes);
            AxXLim = get(Data.handles.ImageAxes, 'XLim');
            AxYLim = get(Data.handles.ImageAxes, 'YLim');
            PointNow = get(ImageFig, 'CurrentPoint');

            CheckX = AxPost(1) <= PointNow(1) && PointNow(1) <= AxPost(1) + AxPost(3);
            CheckY = AxPost(2) <= PointNow(2) && PointNow(2) <= AxPost(2) + AxPost(4);
            
            if CheckX && CheckY
                PointForAxes(1) = round(AxXLim(1) + (PointNow(1)-AxPost(1)).*(diff(AxXLim)/AxPost(3)));
                PointForAxes(2) = round(AxYLim(1) + (PointNow(2)-AxPost(2)).*(diff(AxYLim)/AxPost(4)));

                WhichROI = find(Data.ROIHandle == get(hittest, 'Parent'));
                
                if ~isempty(WhichROI)
                    ObjProp.BackgroundColor = Data.TextColor;
                    Data.ROIobj(WhichROI).setString(Data.ROItext{WhichROI}, ObjProp);

                    m = 1:length(Data.ROIobj);
                    m(m == WhichROI) = [];
                    for k = m;
                        Data.ROIobj(k).setString([]);
                    end
%                     
                else
                    for k = 1:length(Data.ROIobj)
                        try
                        Data.ROIobj(k).setString([]);
                        Data.ROIobj(k).setColor([.4 1 .2]);
                        catch ErrorMsg
                            ClearOutROI;
                            break
                        end
                        
                    end
                end
                
            end
            
        end

        guidata(VisFig, Data);
        
    end

%%%%%




%%%%%
    
    function SilenceMouseover(varargin)
        
        Data = guidata(VisFig);
        
        Data.ROItext(~Data.ROIobj.isvalid()) = [];
        Data.ROIHandle(~Data.ROIobj.isvalid()) = [];
        Data.ROIobj(~Data.ROIobj.isvalid()) = [];

        for k = 1:length(Data.ROIobj)
            Data.ROIobj(k).setString([]);
            Data.ROIobj(k).setColor([.4 1 .2]);
        end
        
        guidata(VisFig, Data);
        
    end

%%%%%

    function RecalcStats(varargin)
        
       %  disp('Recalc stats')
%         disp(get(hittest, 'Parent'))
        
        Data = guidata(VisFig);
        
        ROIMask = zeros(size(Data.Images, 1), size(Data.Images, 2));
        
        for k = 1:length(Data.ROIobj)

                ROIMask = Data.ROIobj(k).createMask();
                ImgMasked = Data.Images(ROIMask);
                Data.ROItext{k} = sprintf('ROI %02d\nMean: %.2f\nMedian: %.2f\nMin: %.2f\nMax: %.2f\nStd Dev: %.2f',...
                    k, mean(ImgMasked(:)), median(ImgMasked(:)), min(ImgMasked(:)), ...
                    max(ImgMasked(:)), std(ImgMasked(:)));

        end

        Data.Mask = flipud(bwlabel(ROIMask));
        
                WhichROI = find(Data.ROIHandle == get(hittest, 'Parent'));
                
                if ~isempty(WhichROI)
                    ObjProp.BackgroundColor = Data.TextColor;
                    Data.ROIobj(WhichROI).setString(Data.ROItext{WhichROI}, ObjProp);

                    m = 1:length(Data.ROIobj);
                    m(m == WhichROI) = [];
                    for k = m;
                        Data.ROIobj(k).setString([]);
                    end
%                     
                else
                    for k = 1:length(Data.ROIobj)
                        Data.ROIobj(k).setString([]);
                    end
                end
        
        
        set(ImageFig, 'WindowButtonMotionFcn', @MouseoverROI);
        guidata(VisFig, Data);
        
    end

%%%%%

    function ClearOutROI(varargin)
        
        Data = guidata(VisFig);
        
        Data.ROItext(~Data.ROIobj.isvalid()) = [];
        Data.ROIHandle(~Data.ROIobj.isvalid()) = [];
        Data.ROIobj(~Data.ROIobj.isvalid()) = [];
        
        guidata(VisFig, Data);
        
    end

%%%%%

    function PlotPtsFcn(varargin)
        
        Data = guidata(VisFig);
        
        [fname, pname, filtIndex] = uiputfile({'*.xls','Excel File (*.xls)'; ...
        '*.csv','CSV File (*.csv)'; ...
        '*.tif',  'TIFF File (*.tif)'}, ...
        'Pick a file', 'DataExport');
    
        switch filtIndex
            case 1
                % Excel file export
                for k = 1:length(Data.ROIobj);

                    ImgMask = Data.ROIobj(k).createMask;
                    PixelData = num2cell(Data.Images(ImgMask));
                    ExportCell = [sprintf('ROI %03d', k); PixelData];
                    startCell = strcat(char(64+k), '1');
                    xlswrite(fullfile(pname, fname), ExportCell, 1, startCell);

                end

            case 2

                outFile = fopen(fullfile(pname, fname), 'wt');

                PixelData = cell(1, length(Data.ROIobj));
                Header = PixelData;
                % CSV file
                for k = 1:length(Data.ROIobj);
                    
                    ImgMask = Data.ROIobj(k).createMask;
                    Header{k} = sprintf('ROI %03d', k);
                    PixelData{k} = num2cell(Data.Images(ImgMask));
                    
                end
                
                ExpLength = cell2mat(cellfun(@length, PixelData, 'UniformOutput', false));
                
                for m = 1:numel(ExpLength)
                    
                    if ExpLength(m) < max(ExpLength)
                        PixelData{m} = [PixelData{m}; num2cell(repmat(char(1), max(ExpLength) - (ExpLength(m)), 1))];
                    end
                end

                %Write the header:
                fprintf(outFile,'%-7s,',Header{1:end-1});
                fprintf(outFile,'%-7s, \n\r',Header{end});

                %Write the data:
                for m = 1:size(PixelData{1}, 1)
                
                    for p = 1:size(PixelData, 2)
                    
                        
                        if cell2mat(PixelData{p}(m)) == 1
                            fprintf(outFile, ',');
                        else
                            fprintf(outFile, '%-.7f,', cell2mat(PixelData{p}(m)));
                        end
                        
                        
                    end
                    fprintf(outFile, '\n\r');
                end

                fclose(outFile);


            case 3

                % Export figure 
                
                % Make dummy figure with ROIs labeled
                for k = 1:length(Data.ROIobj)
                    Data.ROIobj(k).setString(sprintf('ROI %02d', k));
                end
                
                % Capture image frame
                ImgFrame = getframe(Data.handles.ImageAxes);
                
                % Clear out labels
                for k = 1:length(Data.ROIobj)
                    Data.ROIobj(k).setString([]);
                end

                % Write to file
                imwrite(ImgFrame.cdata, fullfile(pname, fname), 'TIFF');

        end

    end

%%%%%

    function CloseVisFig(varargin)
        
        close(findobj('Tag', 'ImageFig'));
        
    end

%%%%%

    function CloseImageFig(varargin)
        
        close(findobj('Tag', 'VisFig'));
        
    end



end


