function SpermTailMotilityProcessingGUIv02(varargin)

% SpermTailMotilityProcessingGUI - GUI for bulk Fourier analysis of sperm tail
% beating videos.
% Processing code assumes .zvi file input 
%
% Non-standard functions used here:
% imreadBFmeta
% imreadBF
% ZVIHashtable2Structure
% Vector2Colormap_setscale
% uipickfiles
%
% 020515 v02 update - error in Notch filter calculation.  Switched from
% cell to vector address.  BW value was missing.
% 
% 05052015 - Improvements to processing speed using bulk detrending and
% in-lined interp1qr code.  

initializeFig;

    function GUIFig = initializeFig(varargin)


        % Initialize main figure
        GUIFig = figure();
        set(GUIFig, 'Position', [100 100 330 240], 'Tag', 'GUIFig', 'NumberTitle', ...
            'off', 'Menubar', 'none', 'Name', 'SpermTailMotility - Bulk Processing');


        % Initialize Panels
%         AxisPanel = uipanel('Parent', GUIFig, 'Units', 'normalized', ...
%             'Position', [0 0 .8 1], 'Tag', 'AxisPanel');

        ButtonPanel = uipanel('Parent', GUIFig, 'Units', 'normalized', ...
            'Position', [0 0 1 1], 'Tag', 'ButtonPanel');
% 
%         % Initialize Axes
        [PathHere, FileHere] = fileparts(mfilename('fullpath'));
        Logo_filename = 'BMIF_logo.jpg';

        if exist(fullfile(PathHere, Logo_filename), 'file');
            logo = imread(fullfile(PathHere, Logo_filename));
        else
            logo = 255-255*BMIFLogoGenerate;
        end

        ImageAxes = axes('Parent', ButtonPanel, 'Units', 'normalized', ...
            'Position', [0.5677    0.3729    0.4169    0.5678], 'Tag', 'ImageAxes');

        
        HoldImg = image(logo, 'Parent', ImageAxes);

        set(ImageAxes, 'XTick', [], 'YTick', []);
        axis image;
        colormap('gray');

        % Initialize Slider and box

%         AxisSlider = uicontrol('Parent', AxisPanel, 'Style', 'slider', ...
%             'Units', 'normalized', 'Callback', @AxisSliderFcn,...
%             'Position', [.2 .05 .78 .04], 'Min', 1, 'Max', 2, 'Value', 1,...
%             'SliderStep', [0.01 0.10], 'Enable', 'off', 'Tag', 'AxisSlider');
% 
%         AxisListener = addlistener(AxisSlider, 'Value', 'PostSet', @AxisListenerFcn);
% 
%         AxisEditBox = uicontrol('Parent', AxisPanel, 'Style', 'edit', ...
%             'Units', 'normalized', 'Callback', @AxisEditFcn, ...
%             'Position', [.03 .03 .14 .09], 'FontSize', 12,...
%             'String', 'Frame', 'Enable', 'off', 'Tag', 'AxisEditBox');


        % Initialize Buttons and Text Box

        LoadButton = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
            'Units', 'normalized', 'Position', [.02 .79 .53 .19], 'FontSize', 12, ...
            'String', 'Select Files', 'Callback', @LoadPathFcn, 'Tag', 'LoadButton');

%         LoadText = uicontrol('Parent', ButtonPanel, 'Style', 'edit', ...
%             'Units', 'normalized', 'Position', [.05 .805 .9 .05], ...
%             'String', '', 'Enable', 'off', 'Tag', 'LoadText');


        OutputButton = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
            'Units', 'normalized', 'Position', [.02 .57 .53 .19], 'FontSize', 12, ...
            'String', 'Output Location', 'Callback', @SetOutPath, 'Tag', 'LoadButton');

        OutputText = uicontrol('Parent', ButtonPanel, 'Style', 'edit', ...
            'Units', 'normalized', 'Position', [.02 .455 .53 .1], ...
            'String', '', 'Enable', 'off', 'Tag', 'LoadText');

        AddFilter = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
            'Units', 'normalized', 'Position', [.02 .18 .53 .19], 'FontSize', 12,...
            'String', 'Filters', 'Callback', @AddFilterFcn, 'Enable', 'on');
% 
%         SelectPts = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
%             'Units', 'normalized', 'Position', [.05 .2 .9 .1], 'FontSize', 12,...
%             'String', 'Select Point', 'Callback', @SelectPtsFcn, 'Enable', 'off');

        PlotPts = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
            'Units', 'normalized', 'Position', [0.6 .02 .39 .19], 'FontSize', 12, ...
            'String', 'Process Files', 'Callback', @PlotPtsFcn, 'Enable', 'off');


        Data.handles.GUIFig = GUIFig;
%         Data.handles.AxisPanel = AxisPanel;
        Data.handles.ButtonPanel = ButtonPanel;
        Data.handles.ImageAxes = ImageAxes;
%         Data.handles.AxisSlider = AxisSlider;
%         Data.handles.AxisEditBox = AxisEditBox;
        Data.handles.LoadButton = LoadButton;
%         Data.handles.LoadText = LoadText;
%         Data.handles.SelectPts = SelectPts;
        Data.handles.PlotPts = PlotPts;
        Data.handles.OutputButton = OutputButton;
        Data.handles.OutputText = OutputText;
        Data.handles.AddFilter = AddFilter;
        Data.filters.TableData = {};
        Data.filters.LowPass = {};
        Data.filters.Notch = {};
        Data.filters.Comb = {};
        Data.handles.SelectPoint = [];
        Data.handles.TraceFigs = [];
        
        Data.SelectedFiles = {};

        guidata(GUIFig, Data);

    end

%%%%%

    function LoadPathFcn(varargin)
        
        Data = guidata(findobj('Tag', 'GUIFig'));
        
        selectFiles = uipickfiles('FilterSpec', pwd, 'Type', { '*.zvi',   'Zeiss Axiovision' }, 'Append', Data.SelectedFiles);
        
        if iscell(selectFiles)
            Data.SelectedFiles = selectFiles;

            % Change button status

            set(Data.handles.PlotPts, 'String', sprintf('Process %d Files', length(Data.SelectedFiles)));
            set(Data.handles.AddFilter, 'Enable', 'on');

            guidata(Data.handles.GUIFig, Data);
        end
    end

%%%%%

    function SetOutPath(varargin)
        
        Data = guidata(findobj('Tag', 'GUIFig'));
        
        [PathHere, FileHere] = fileparts(mfilename('fullpath'));
        
        directoryname = uigetdir(PathHere);
        
        if isequal(directoryname,0)           
            
        else
            set(Data.handles.OutputText, 'String', directoryname, 'Enable', 'on');
            
            
%             if strcmp(get(Data.handles.LoadText, 'Enable'), 'on')
                set(Data.handles.PlotPts,'Enable', 'on')
%             end
            
        end
        
    end

%%%%%

    function AddFilterFcn(varargin)
        
        GUIFig = findobj('Tag', 'GUIFig');
        Data = guidata(GUIFig);
        
%         disp('Addfilter')
        
        % Launch new filter-editing window
        Data.handles.FilterFig = figure();
        GUIPosition = get(Data.handles.GUIFig, 'Position');
        set(Data.handles.FilterFig, 'Position', [(GUIPosition(1) + GUIPosition(3)/2 - 150) (GUIPosition(2) + GUIPosition(4)/2 - 140) 300 280], ...
           'Resize', 'off', 'Tag', 'FilterFig', 'DeleteFcn', @DeleteFilterWindow, ...
           'Toolbar', 'none', 'Menubar', 'none', 'Name', 'Filter Settings', 'NumberTitle', 'off');
        
        % Initialize Panels
        Data.handles.FiltListPanel = uipanel('Parent', Data.handles.FilterFig, 'Units', 'normalized', ...
            'Position', [0 .5 1 .5], 'Tag', 'FilterListPanel');

        Data.handles.FiltButtonPanel = uipanel('Parent', Data.handles.FilterFig, 'Units', 'normalized', ...
            'Position', [0 0 1 .5], 'Tag', 'FiltButtonPanel');
        
        % Initialize Table
        
        Data.handles.FiltTable = uitable('Parent', Data.handles.FiltListPanel, 'Units', 'normalized', ...
            'Position', [.05 .05 .9 .9], 'Data', Data.filters.TableData, 'ColumnName', 'Filter Name');
        
        TableSize = getpixelposition(Data.handles.FiltTable);
        
        set(Data.handles.FiltTable, 'ColumnWidth', {TableSize(3)*.88}, 'CellSelectionCallback', @(src,evnt)set(src,'UserData',evnt.Indices))
        
        % Initialize Buttons
        
        Data.handles.AddFiltButton = uicontrol('Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', 'tooltipstring', 'Add Filter',...
            'Position', [0.0131    0.6600    0.1000    0.2130], 'String', '+', 'FontSize', 18, 'Style', 'pushbutton', 'Callback', @AddFilterPush);
        
        Data.handles.DeleteFiltButton = uicontrol('Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', 'tooltipstring', 'Delete Filter',...
            'Position', [0.0131 .42 .1 .213], 'String', 'X', 'FontWeight', 'Bold', 'FontSize', 11, 'Style', 'pushbutton', 'Callback', @DeleteFilterPush);
        
        Data.handles.CloseFiltbutton = uicontrol('Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.765 .03 .2 .25], 'String', 'OK', 'Style', 'pushbutton', 'Callback', @CloseFiltWindow);
     
        % Initialize Drop-down
        
        Data.handles.FilterDropDown = uicontrol('Style', 'popup', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.1261    0.7500    0.3000    0.1000], 'String', {'High Pass', 'Notch', 'Comb'}, 'BackgroundColor', [1 1 1], 'Callback', @ChangeFilterType);
        
        % Define all text boxes, then make visible the ones you need at the
        % time
        
        Data.handles.HighPassText = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.5366    0.6324    0.2400    0.2350], 'String', 'High Pass Cut-on (Hz):', 'Visible', 'on');
        
        Data.handles.HighPassEdit = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.8    0.6691    0.18   0.17], 'String', num2str(2), 'BackgroundColor', [1 1 1], 'Visible', 'on');
        
        %
        Data.handles.NotchTextCenter = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4727    0.8525    0.3250    0.1000], 'String', 'Filter Position (Hz):', 'Visible', 'off');
        
        Data.handles.NotchEditCenter = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .81 .18 .17], 'String', num2str(2), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        Data.handles.NotchTextWidth = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4324    0.5131    0.3543    0.1400], 'String', 'Filter Bandwidth (Hz):', 'Visible', 'off');
        
        Data.handles.NotchEditWidth = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .511 .18 .17], 'String', num2str(0.1), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        %
        
        Data.handles.CombTextCenter = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4727    0.8525    0.3250    0.1000], 'String', 'Harmonic Number:', 'Visible', 'off');
        
        Data.handles.CombTextEdit = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .81 .18 .17], 'String', num2str(2), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        Data.handles.CombTextWidth = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4324    0.5131    0.3543    0.1400], 'String', 'Filter Bandwidth (Hz):', 'Visible', 'off');
        
        Data.handles.CombEditWidth = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .511 .18 .17], 'String', num2str(0.1), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        %%%
        
        Data.handles.MaxFreqToPlotText = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.0095    0.0662    0.2743    0.2488], 'String', 'Max Plot Frequency (Hz):', 'Visible', 'on');
        
        Data.handles.MaxFreqToPlotEdit =  uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.3250    0.1191    0.1800    0.1700], 'String', num2str(22), 'BackgroundColor', [1 1 1], 'Visible', 'on');
        
        
        
        % Close out Init
        
        guidata(GUIFig, Data);
        
        function AddFilterPush(varargin)
            
            Data = guidata(GUIFig);
            
            whichType = get(Data.handles.FilterDropDown, 'Value');
            
            switch whichType
                
                case 1 % High-pass filter
                    
                    HighPassCutOn = str2double(get(Data.handles.HighPassEdit, 'String'));
                    
                    if (HighPassCutOn > 0) && (HighPassCutOn < 30)

                        Data.filters.LowPass{numel(Data.filters.LowPass)+1}{1} = HighPassCutOn;

                        tabData = get(Data.handles.FiltTable, 'Data');
                        tabData(cellfun(@isempty, tabData)) = [];
                        
                        addString = sprintf('High-pass @ %s Hz', get(Data.handles.HighPassEdit, 'String'));
                        
                        if any(strcmp(addString, tabData))
                            msgbox('Identical filter already specified.', 'Filter Error!');
                        else
                        
                            tabData{length(tabData) + 1, 1} = addString;
                            set(Data.handles.FiltTable, 'Data', tabData);
                        
                        end
                    else
                        
                        msgbox('Filter cut-on must be greater than 0 and less than frame rate.', 'Filter Error!');
                    end
                    
                case 2 % Notch filter
                    

                    NotchFilt = str2double(get(Data.handles.NotchEditCenter, 'String'));
                    Bandwidth = str2double(get(Data.handles.NotchEditWidth, 'String'));
                    
%                     NotchFiltWidth = (NotchFilt/((Data.FrameRate/2)*Bandwidth));
                    
                    if (NotchFilt > 0) && (Bandwidth > 0)
                        
                        nextNotch = numel(Data.filters.Notch)+1;

                        Data.filters.Notch{nextNotch}(1) = NotchFilt;
                        Data.filters.Notch{nextNotch}(2) = Bandwidth;
                        
                        addString = sprintf('Notch @ %s / %.3d Hz', get(Data.handles.NotchEditCenter, 'String'), Bandwidth);
                        
                        tabData = get(Data.handles.FiltTable, 'Data');
                        tabData(cellfun(@isempty, tabData)) = [];
                        
                        if any(strcmp(addString, tabData))
                            msgbox('Identical filter already specified.', 'Filter Error!');
                        else
                        
                            tabData{length(tabData) + 1, 1} = addString;
                            set(Data.handles.FiltTable, 'Data', tabData);
                        
                        end
                        
                    else
                        msgbox(sprintf('Filter center must be greater than 0 and less than (frame rate)/2. \nFilter width must be greater than 0.'), 'Filter Error!');
                    end
                    
                case 3 % Comb filter

                    CombCenter = str2double(get(Data.handles.CombTextEdit, 'String'));
                    Bandwidth = str2double(get(Data.handles.CombEditWidth, 'String'));

%                     CombWidth = (Bandwidth/(Data.FrameRate));
                    
                    if (CombCenter > 0) && (Bandwidth > 0)
  
                       nextComb = numel(Data.filters.Comb)+1;

                        Data.filters.Comb{nextComb}(1) = CombCenter;
                        Data.filters.Comb{nextComb}(2) = Bandwidth; 
                        
                        addString = sprintf('Comb @ (F / %d) / %.3d Hz', CombCenter, Bandwidth);
                        
                        tabData = get(Data.handles.FiltTable, 'Data');
                        tabData(cellfun(@isempty, tabData)) = [];
                        
                        if any(strcmp(addString, tabData))
                            msgbox('Identical filter already specified.', 'Filter Error!');
                        else
                        
                            tabData{length(tabData) + 1, 1} = addString;
                            set(Data.handles.FiltTable, 'Data', tabData);
                        
                        end
                        
                    else
                        msgbox(sprintf('Filter harmonic must be greater than 0 and less than frame rate. \nFilter width must be greater than 0.'), 'Filter Error!');
                    end
                    
            end
            
            Data.filters.TableData = tabData;
            
            guidata(GUIFig, Data)
            
        end
        
        %%%
        
        function DeleteFilterPush(varargin)
            
            Data = guidata(GUIFig);
            whichFilter = get(Data.handles.FiltTable, 'UserData');
            whichFilter = whichFilter(1);
            
            dt = get(Data.handles.FiltTable, 'Data');
            
            % Determine filter type here
            FilterIs = find([~isempty(strfind(dt{whichFilter}, 'High-pass')), ...
                ~isempty(strfind(dt{whichFilter}, 'Notch')), ~isempty(strfind(dt{whichFilter}, 'Comb'))]);
            
            FilterTypes = {'High-pass', 'Notch', 'Comb'};
            
            SameTypeFilters = find(~cell2mat(cellfun(@isempty, strfind(dt, FilterTypes{FilterIs}), 'UniformOutput', false)'));
            
            % Clear out that filter
            
            switch FilterIs
                
                case 1 % High-pass filter
                    
                    Data.filters.LowPass(SameTypeFilters == whichFilter) = [];
                    
                case 2 % Notch filter
                    
                    Data.filters.Notch(SameTypeFilters == whichFilter) = [];
                    
                case 3 % Comb filter
                    
                    Data.filters.Comb(SameTypeFilters == whichFilter) = [];
                    
            end
            
            Data.filters.TableData(whichFilter) = [];
                        
            % Redraw table without deleted filter
            
            delete(Data.handles.FiltTable)
            Data.handles.FiltTable = uitable('Parent', Data.handles.FiltListPanel, 'Units', 'normalized', ...
                'Position', [.05 .05 .9 .9], 'Data', Data.filters.TableData, 'ColumnName', 'Filter Name');
        
            TableSize = getpixelposition(Data.handles.FiltTable);
        
            set(Data.handles.FiltTable, 'ColumnWidth', {TableSize(3)*.88}, 'CellSelectionCallback', @(src,evnt)set(src,'UserData',evnt.Indices))
            
            guidata(GUIFig, Data);
            
        end
        
        %%%
        
        function ChangeFilterType(varargin)
            
            whichType = get(varargin{1}, 'Value');
            
            switch whichType
                
                case 1 
                    set([Data.handles.HighPassText, Data.handles.HighPassEdit], 'Visible', 'on');
                    set([Data.handles.NotchTextCenter, Data.handles.NotchEditCenter, ...
                        Data.handles.NotchTextWidth, Data.handles.NotchEditWidth, ...
                        Data.handles.CombTextCenter, Data.handles.CombTextEdit, ...
                        Data.handles.CombTextWidth, Data.handles.CombEditWidth], 'Visible', 'off');
                    
                case 2 
                    set([Data.handles.NotchTextCenter, Data.handles.NotchEditCenter, ...
                        Data.handles.NotchTextWidth, Data.handles.NotchEditWidth], 'Visible', 'on');
                    set([Data.handles.HighPassText, Data.handles.HighPassEdit, ...
                        Data.handles.CombTextCenter, Data.handles.CombTextEdit, ...
                        Data.handles.CombTextWidth, Data.handles.CombEditWidth], 'Visible', 'off');
                    
                case 3
                    set([Data.handles.CombTextCenter, Data.handles.CombTextEdit, ...
                        Data.handles.CombTextWidth, Data.handles.CombEditWidth], 'Visible', 'on');
                    set([Data.handles.HighPassText, Data.handles.HighPassEdit, ...
                        Data.handles.NotchTextCenter, Data.handles.NotchEditCenter, ...
                        Data.handles.NotchTextWidth, Data.handles.NotchEditWidth], 'Visible', 'off');
            end
            
        end
        
        %%%
        
        function CloseFiltWindow(varargin)

            close(Data.handles.FilterFig);

        end

        %%%

        function DeleteFilterWindow(varargin)

            % Save filter specs to use again
    %         disp('Save and close')

            Data.filters.TableData = get(Data.handles.FiltTable, 'Data');
            Data.filters.MaxFreqToPlot = str2double(get(Data.handles.MaxFreqToPlotEdit, 'String'));
            guidata(GUIFig, Data);

        end
        
        
    end

%%%%%

    function PlotPtsFcn(varargin)
        
        GUIFig = findobj('Tag', 'GUIFig');
        Data = guidata(GUIFig);
        
        OnButtons = findobj('Parent', Data.handles.ButtonPanel, 'Enable', 'on', 'Style', 'pushbutton');
        set(OnButtons, 'Enable', 'off')
        set(Data.handles.GUIFig, 'pointer', 'watch');
        drawnow;
        

        
        % Loop over selected files

        for f = 1:length(Data.SelectedFiles)
            fprintf(1, 'Analysis started at %s.\n', datestr(now));
            tic;
            fileName = Data.SelectedFiles{f};
            
            [~, fileNameOnly] = fileparts(fileName);
            
            fileNameOnly = strrep(fileNameOnly, '_', '\_');
            
             w = waitbar(0, sprintf('Processing %s...', fileNameOnly));
             waitbar((f-1)/length(Data.SelectedFiles), w);
        
            Data.ImgMetaData = imreadBFmeta(fileName);
            Data.VidFrames = squeeze(imreadBF(fileName, 1, 1:Data.ImgMetaData.nframes, 1));
            
            Data.metaData = ZVIHashtable2Structure(Data.ImgMetaData);
%             Data.NFrames = str2double(Data.metaData.Image.CountT);
            Data.NFrames = numel(Data.metaData.Timestamp);
            Data.FrameRate = (numel(Data.metaData.Timestamp)-1)*1000/Data.metaData.RelTimestamp(end);
            Data.TimeVector = (0:(1000/Data.FrameRate):((Data.NFrames-1)*(1000/Data.FrameRate)))';
            
            zeroPadLength = 2^(1+nextpow2(Data.NFrames));
            Data.TimeDomain = linspace(0,1,(zeroPadLength/2 + 1))*(Data.FrameRate/2);
        
            guidata(GUIFig, Data);
            
            FFTImg = zeros(Data.ImgMetaData.height, Data.ImgMetaData.width);
            FFTAmp = FFTImg;
            VarImg = FFTImg;
        
            fprintf(1, 'Data loaded in %.2f seconds.\n', toc)
            %%%%%%%%%%%
            % Generate filters here          

            FiltSpecsLowPass = cell(length(Data.filters.LowPass), 2);
            for lp = 1:length(Data.filters.LowPass)
                fNorm = max([0 Data.filters.LowPass{lp}{1}/(Data.FrameRate/2)]);

                if fNorm > 0
                    [bB,aB] = butter(10, fNorm, 'high');
                end

                FiltSpecsLowPass{lp, 1} = bB;
                FiltSpecsLowPass{lp, 2} = aB;
            end

            FiltSpecsNotch = cell(length(Data.filters.Notch), 2);
            for fn = 1:length(Data.filters.Notch)
                NotchFiltWidth = (Data.filters.Notch{fn}(1)/((Data.FrameRate/2)*Data.filters.Notch{fn}(2)));
                Wo = Data.filters.Notch{fn}(1)/(Data.FrameRate/2);
                BW = Wo/NotchFiltWidth;
                [b1,a1] = iirnotch(Wo,BW);

                FiltSpecsNotch{fn, 1} = b1;
                FiltSpecsNotch{fn, 2} = a1;
            end

            FiltSpecsComb = cell(length(Data.filters.Comb), 2);
            for fc = 1:length(Data.filters.Comb)
                CombWidth = Data.filters.Comb{fc}(1)*(Data.filters.Comb{fc}(2)/(Data.FrameRate));
                [bC,aC] = iircomb(Data.filters.Comb{fc}(1),CombWidth, 'notch'); % Comb filter a 4th harmonic of Frame Rate

                FiltSpecsComb{fc, 1} = bC;
                FiltSpecsComb{fc, 2} = aC;
            end
            
            %%%%%%%%%%%
            
            % Reshape and detrend entire data set
            Dt = reshape(Data.VidFrames, (size(Data.VidFrames, 1)*size(Data.VidFrames, 2)), size(Data.VidFrames, 3));
            Dt = detrend(Dt, 'constant');
            
            %%%%%%%%%
            % Calc time axis for fast linear interpolation.  Based on interp1qr.  Can do this since many
            % of these variables are the same across all pixels.

            % For each 'xi', get the position of the 'x' element bounding it on the left [p x 1]
            [~,xi_pos] = histc(Data.TimeVector, Data.metaData.RelTimestamp);
            xi_pos = max(xi_pos, 1);     % To avoid index=0 when xi < x(1)
            xi_pos = min(xi_pos, size(Data.metaData.RelTimestamp, 1)-1);   % To avoid index=m+1 when xi > x(end).

            % 't' matrix [p x 1]
            dxi = Data.metaData.RelTimestamp-Data.TimeVector(xi_pos);
            dx = Data.TimeVector(xi_pos+1)-Data.TimeVector(xi_pos);
            t = dxi./dx;

            % End interp1qr code.  yi calculate on a per-pixel basis in loop
            % below.  Total time cut down by ~40%.  Hair under 2 min for an
            % entire data set.  
            %%%%%%%%%%%%%%%%%%%%%%    
        
        
            zeroPadLength = 2^(1+nextpow2(Data.NFrames));
            

            fprintf(1, 'Pre-processing at %.2f seconds.\n', toc)
            for pix = 1:numel(FFTImg)
                
                if mod(pix, 1000) == 0
                    waitbar(((f-1)/length(Data.SelectedFiles)) + ((1/length(Data.SelectedFiles))*(pix/numel(FFTImg))), w);
                end
%                 [p1, p2] = ind2sub([Data.ImgMetaData.height, Data.ImgMetaData.width], pix);

                % Filter trace here

%                 filtvectRaw = squeeze((Data.VidFrames(p1, p2, :) - mean(Data.VidFrames(p1, p2, :))))';
                filtvectRaw = Dt(pix, :)';
                
                % Take filtvect and resample it to have a constant time axis
%                 filtvect = interp1(Data.metaData.RelTimestamp, filtvectRaw, Data.TimeVector);
%                 filtvect(end) = [];
                
                filtvect = filtvectRaw(xi_pos,:) + t(:, 1).*(filtvectRaw(xi_pos+1,:)-filtvectRaw(xi_pos,:));
                filtvect(Data.TimeVector<Data.metaData.RelTimestamp(1) | Data.TimeVector>Data.metaData.RelTimestamp(end),:) = NaN;
                filtvect(end) = [];


                if (numel(FiltSpecsLowPass) + numel(FiltSpecsNotch) + numel(FiltSpecsComb)) > 0

                    for fn = 1:size(FiltSpecsLowPass, 1)
                        filtvect = filtfilt(FiltSpecsLowPass{fn,1}, FiltSpecsLowPass{fn,2}, filtvect); % Low pass
                    end

                    for fn = 1:size(FiltSpecsNotch, 1)
                        filtvect = filtfilt(FiltSpecsNotch{fn,1}, FiltSpecsNotch{fn,2}, filtvect); % Notch filters
                    end

                    for fn = 1:size(FiltSpecsComb, 1)
                        filtvect = filtfilt(FiltSpecsComb{fn,1}, FiltSpecsComb{fn,2}, filtvect); % Notch filters
                    end

                end

                    outFFT = (fft(filtvect, zeroPadLength))/(Data.NFrames-1);
                    outVar = var(filtvect);
                    outHalf = 2*abs(outFFT(1:(zeroPadLength/2 + 1)));
                    FFTImg(pix) = Data.TimeDomain(find(outHalf == max(outHalf), 1, 'first'));
                    FFTAmp(pix) = log(max(outHalf).^2);
                    VarImg(pix) = outVar; 

            end
        
            waitbar(1, w);
            close(w);
        
            fprintf(1, 'Analysis at %.2f seconds.\n', toc)
            %%%%%%%%%%%
            % Plotting


            FreqCutoff = Data.filters.MaxFreqToPlot;
            ColorMapIt = Vector2Colormap_setscale((FFTImg), 'jet', [0 FreqCutoff]).*Vector2Colormap_setscale((FFTAmp)/max(FFTAmp(:)), 'gray', [0 1]);
% 
            close(findobj('Tag', 'outputFFTImg'));
            dispFig = figure();
            set(dispFig, 'Toolbar', 'none', 'Menubar', 'none', 'Tag', 'outputFFTImg');
            image(ColorMapIt)
            set(gca, 'XTick', []);
            set(gca, 'YTick', []);
            axis image
            title('Color - Frequency; Intensity - Amplitude', 'FontSize', 12);
            c = colorbar;
            ylabel(c, 'Frequency (Hz)', 'FontSize', 12);
            set(c, 'YTick', 0:8:64);
            set(c, 'YTickLabel', num2str(((0:8:64)*(FreqCutoff/64))'));
            set(c, 'FontSize', 12);
            

%             figure(14)
%             imagesc(FFTImg)
%             title('FFT peak in time domain');
            

            
            
            
            FilterTable = Data.filters.TableData;
            
            saveName = sprintf('%s_%s.mat', 'FFT_Results', fileNameOnly);
            
            save(fullfile(get(Data.handles.OutputText, 'String'), saveName), 'FFTImg', 'FFTAmp', 'VarImg', 'FreqCutoff', 'FilterTable');
        
        end
        
  
        
            set(OnButtons, 'Enable', 'on')
            set(Data.handles.GUIFig, 'pointer', 'arrow');
            drawnow;
        
        
    end

%%%%%


end

