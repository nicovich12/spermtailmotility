function SpermTailMotilityProcessingSinglev02

% SpermTailMotilityProcessingSingle - GUI for single-point Fourier analysis of sperm tail
% beating videos.
% Processing code assumes .zvi file input 
%
% Non-standard functions used here:
% imreadBFmeta
% imreadBF
% ZVIHashtable2Structure
% Vector2Colormap_setscale
% uipickfiles
% sortcell

% 28042015 - Update uses fewer calls to interp1 (with improved fcn) and to
% waitbar.  Stack processing speed greatly increased.

% Initialize main figure
GUIFig = figure();
set(GUIFig, 'Position', [100 100 590 480], 'Tag', 'GUIFig', 'NumberTitle', ...
    'off', 'Menubar', 'none', 'Name', 'SpermTailMotility - Single Dataset');


% Initialize Panels
AxisPanel = uipanel('Parent', GUIFig, 'Units', 'normalized', ...
    'Position', [0 0 .7 1], 'Tag', 'AxisPanel');

ButtonPanel = uipanel('Parent', GUIFig, 'Units', 'normalized', ...
    'Position', [.7 0 .3 1], 'Tag', 'ButtonPanel');

% Initialize Axes
[PathHere, FileHere] = fileparts(mfilename('fullpath'));
Logo_filename = 'BMIF_logo.jpg';

if exist(fullfile(PathHere, Logo_filename), 'file')
	logo = imread(fullfile(PathHere, Logo_filename));
else
	logo = 255-(255*BMIFLogoGenerate);
end

ImageAxes = axes('Parent', AxisPanel, 'Units', 'normalized', ...
    'Position', [.01 .15 .98 .84], 'Tag', 'ImageAxes');
HoldImg = image(logo, 'Parent', ImageAxes);

set(ImageAxes, 'XTick', [], 'YTick', []);
axis image;
colormap('gray');

% Initialize Slider and box

AxisSlider = uicontrol('Parent', AxisPanel, 'Style', 'slider', ...
    'Units', 'normalized', 'Callback', @AxisSliderFcn,...
    'Position', [.2 .05 .78 .04], 'Min', 1, 'Max', 2, 'Value', 1,...
    'SliderStep', [0.01 0.10], 'Enable', 'off', 'Tag', 'AxisSlider');

AxisListener = addlistener(AxisSlider, 'Value', 'PostSet', @AxisListenerFcn);

AxisEditBox = uicontrol('Parent', AxisPanel, 'Style', 'edit', ...
    'Units', 'normalized', 'Callback', @AxisEditFcn, ...
    'Position', [.03 .03 .14 .09], 'FontSize', 12,...
    'String', 'Frame', 'Enable', 'off', 'Tag', 'AxisEditBox');


% Initialize Buttons and Text Box

LoadButton = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [.05 .87 .9 .1], 'FontSize', 12, ...
    'String', 'Load File', 'Callback', @LoadPathFcn, 'Tag', 'LoadButton');

LoadText = uicontrol('Parent', ButtonPanel, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [.05 .805 .9 .05], ...
    'String', '', 'Enable', 'off', 'Tag', 'LoadText');


OutputButton = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [.05 .67 .9 .1], 'FontSize', 12, ...
    'String', 'Output Location', 'Callback', @SetOutPath, 'Tag', 'LoadButton');

OutputText = uicontrol('Parent', ButtonPanel, 'Style', 'edit', ...
    'Units', 'normalized', 'Position', [.05 .605 .9 .05], ...
    'String', '', 'Enable', 'off', 'Tag', 'LoadText');

AddFilter = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [.05 .35 .9 .1], 'FontSize', 12,...
    'String', 'Filters', 'Callback', @AddFilterFcn, 'Enable', 'off');

SelectPts = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [.05 .2 .9 .1], 'FontSize', 12,...
    'String', 'Select Point', 'Callback', @SelectPtsFcn, 'Enable', 'off');

PlotPts = uicontrol('Parent', ButtonPanel, 'Style', 'pushbutton', ...
    'Units', 'normalized', 'Position', [0.05 .05 .9 .1], 'FontSize', 12, ...
    'String', 'Process Stack', 'Callback', @PlotPtsFcn, 'Enable', 'off');


Data.handles.GUIFig = GUIFig;
Data.handles.AxisPanel = AxisPanel;
Data.handles.ButtonPanel = ButtonPanel;
Data.handles.ImageAxes = ImageAxes;
Data.handles.AxisSlider = AxisSlider;
Data.handles.AxisEditBox = AxisEditBox;
Data.handles.LoadButton = LoadButton;
Data.handles.LoadText = LoadText;
Data.handles.SelectPts = SelectPts;
Data.handles.PlotPts = PlotPts;
Data.handles.OutputButton = OutputButton;
Data.handles.OutputText = OutputText;
Data.handles.AddFilter = AddFilter;
Data.filters.TableData = {};
Data.filters.LowPass = {};
Data.filters.Notch = {};
Data.filters.Comb = {};
Data.handles.SelectPoint = [];
Data.handles.TraceFigs = [];

guidata(GUIFig, Data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback functions

%%%%%

    function AxisSliderFcn(varargin)
        
%         SliderValue = get(AxisSlider, 'Value');
%         
%         set(AxisSlider, 'Value', round(SliderValue));
%         set(AxisEditBox, 'String', num2str(round(SliderValue)));
%         
%         DisplayImageInAxes;
        
    end

%%%%%

    function AxisListenerFcn(varargin)
        
        SliderValue = get(AxisSlider, 'Value');
        
        set(AxisSlider, 'Value', round(SliderValue));
        set(AxisEditBox, 'String', num2str(round(SliderValue)));
        
        DisplayImageInAxes;
        
    end

%%%%%

    function AxisEditFcn(varargin)
        
        EditHere = str2double(get(AxisEditBox, 'String'));
        
        EditHere = round(EditHere);
        
        EditHere = min([EditHere get(AxisSlider, 'Max')]);
        EditHere = max([1 EditHere]);
        
        set(AxisEditBox, 'String', num2str(EditHere));
        set(AxisSlider, 'Value', EditHere);
        
        DisplayImageInAxes;
        
    end

%%%%%

    function LoadPathFcn(varargin)

        Data = guidata(findobj('Tag', 'GUIFig'));
        
        [PathHere, FileHere] = fileparts(mfilename('fullpath'));
        
        [filename, pathname] = uigetfile({'*.zvi', 'Zeiss AxioVision File'}, PathHere);
        
        if isequal(filename,0) || isequal(pathname,0)
            
        else
        
            OnButtons = findobj('Parent', Data.handles.ButtonPanel, 'Enable', 'on', 'Style', 'pushbutton');
            set(OnButtons, 'Enable', 'off')
            set(Data.handles.GUIFig, 'pointer', 'watch');
            drawnow;
            
            Data.ImgMetaData = imreadBFmeta(fullfile(pathname, filename));
            Data.VidFrames = squeeze(imreadBF(fullfile(pathname, filename), 1, 1:Data.ImgMetaData.nframes, 1));
            
            % Use bfopen here to speed things up?  Reduce memory usage?
            % Seems to do neither
%              DataHere = bfopen(fullfile(pathname, filename));
            
            
%             Data.metaData = ZVIHashtable2Structure(DataHere{2});
            
            Data.metaData = ZVIHashtable2Structure(Data.ImgMetaData);
            Data.NFrames = str2double(Data.metaData.Image.CountT);
            Data.FrameRate = (str2double(Data.metaData.Image.CountT)-1)*1000/Data.metaData.RelTimestamp(end);
            Data.TimeVector = (0:(1000/Data.FrameRate):((Data.NFrames-1)*(1000/Data.FrameRate)))';
            
            zeroPadLength = 2^(1+nextpow2(Data.NFrames));
            Data.TimeDomain = linspace(0,1,(zeroPadLength/2 + 1))*(Data.FrameRate/2);

            set(Data.handles.LoadText, 'String', filename, ...
                'Enable', 'on');
            
            if strcmp(get(Data.handles.OutputText, 'Enable'), 'on')
                set(Data.handles.PlotPts,'Enable', 'on')
            end
            
            sldStp = 1/(size(Data.VidFrames, 3) - 1);
            set(Data.handles.AxisSlider, 'Max', size(Data.VidFrames, 3), 'Min', 1,...
                'Enable', 'on', 'SliderStep', [sldStp, 10*sldStp]);
            set(Data.handles.AxisEditBox, ...
                'String', num2str(get(Data.handles.AxisSlider, 'Value')), ...
                'Enable', 'on');
            set(Data.handles.SelectPts, 'Enable', 'on')
            set(Data.handles.AddFilter, 'Enable', 'on')

            set(OnButtons, 'Enable', 'on')
            set(Data.handles.GUIFig, 'pointer', 'arrow');
            drawnow;

            guidata(findobj('Tag', 'GUIFig'), Data);

            DisplayImageInAxes;
        end
        
    end

%%%%%

    function SetOutPath(varargin)
        
        Data = guidata(findobj('Tag', 'GUIFig'));
        
        [PathHere, FileHere] = fileparts(mfilename('fullpath'));
        
        directoryname = uigetdir(PathHere);
        
        if isequal(directoryname,0)           
            
        else
            set(Data.handles.OutputText, 'String', directoryname, 'Enable', 'on');
            
            
            if strcmp(get(Data.handles.LoadText, 'Enable'), 'on')
                set(Data.handles.PlotPts,'Enable', 'on')
            end
            
        end
        
    end

%%%%%
    function AddFilterFcn(varargin)
        
        Data = guidata(GUIFig);
        
%         disp('Addfilter')
        
        % Launch new filter-editing window
        Data.handles.FilterFig = figure();
        GUIPosition = get(Data.handles.GUIFig, 'Position');
        set(Data.handles.FilterFig, 'Position', [(GUIPosition(1) + GUIPosition(3)/2 - 150) (GUIPosition(2) + GUIPosition(4)/2 - 140) 300 280], ...
           'Resize', 'off', 'Tag', 'FilterFig', 'DeleteFcn', @DeleteFilterWindow, ...
           'Toolbar', 'none', 'Menubar', 'none', 'Name', 'Filter Settings', 'NumberTitle', 'off');
        
        % Initialize Panels
        Data.handles.FiltListPanel = uipanel('Parent', Data.handles.FilterFig, 'Units', 'normalized', ...
            'Position', [0 .5 1 .5], 'Tag', 'FilterListPanel');

        Data.handles.FiltButtonPanel = uipanel('Parent', Data.handles.FilterFig, 'Units', 'normalized', ...
            'Position', [0 0 1 .5], 'Tag', 'FiltButtonPanel');
        
        % Initialize Table
        
        Data.handles.FiltTable = uitable('Parent', Data.handles.FiltListPanel, 'Units', 'normalized', ...
            'Position', [.05 .05 .9 .9], 'Data', Data.filters.TableData, 'ColumnName', 'Filter Name');
        
        TableSize = getpixelposition(Data.handles.FiltTable);
        
        set(Data.handles.FiltTable, 'ColumnWidth', {TableSize(3)*.88}, 'CellSelectionCallback', @(src,evnt)set(src,'UserData',evnt.Indices))
        
        % Initialize Buttons
        
        Data.handles.AddFiltButton = uicontrol('Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', 'tooltipstring', 'Add Filter',...
            'Position', [0.0131    0.6600    0.1000    0.2130], 'String', '+', 'FontSize', 18, 'Style', 'pushbutton', 'Callback', @AddFilterPush);
        
        Data.handles.DeleteFiltButton = uicontrol('Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', 'tooltipstring', 'Delete Filter',...
            'Position', [0.0131 .42 .1 .213], 'String', 'X', 'FontWeight', 'Bold', 'FontSize', 11, 'Style', 'pushbutton', 'Callback', @DeleteFilterPush);
        
        Data.handles.CloseFiltbutton = uicontrol('Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.765 .03 .2 .25], 'String', 'OK', 'Style', 'pushbutton', 'Callback', @CloseFiltWindow);
     
        % Initialize Drop-down
        
        Data.handles.FilterDropDown = uicontrol('Style', 'popup', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.1261    0.6500    0.3000    0.2000], 'String', {'High Pass', 'Notch', 'Comb'}, 'BackgroundColor', [1 1 1], 'Callback', @ChangeFilterType);
        
        % Define all text boxes, then make visible the ones you need at the
        % time
        
        Data.handles.HighPassText = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.5366    0.6324    0.2400    0.2350], 'String', 'High Pass Cut-on (Hz):', 'Visible', 'on');
        
        Data.handles.HighPassEdit = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.8    0.6691    0.18   0.17], 'String', num2str(2), 'BackgroundColor', [1 1 1], 'Visible', 'on');
        
        %
        Data.handles.NotchTextCenter = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4727    0.8525    0.3250    0.1000], 'String', 'Filter Position (Hz):', 'Visible', 'off');
        
        Data.handles.NotchEditCenter = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .81 .18 .17], 'String', num2str(2), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        Data.handles.NotchTextWidth = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4324    0.5131    0.3543    0.1400], 'String', 'Filter Bandwidth (Hz):', 'Visible', 'off');
        
        Data.handles.NotchEditWidth = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .511 .18 .17], 'String', num2str(0.1), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        %
        
        Data.handles.CombTextCenter = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4727    0.8525    0.3250    0.1000], 'String', 'Harmonic Number:', 'Visible', 'off');
        
        Data.handles.CombTextEdit = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .81 .18 .17], 'String', num2str(2), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        Data.handles.CombTextWidth = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.4324    0.5131    0.3543    0.1400], 'String', 'Filter Bandwidth (Hz):', 'Visible', 'off');
        
        Data.handles.CombEditWidth = uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [.8 .511 .18 .17], 'String', num2str(0.1), 'BackgroundColor', [1 1 1], 'Visible', 'off');
        
        %%%
        
        Data.handles.MaxFreqToPlotText = uicontrol('Style', 'text', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.0095    0.0662    0.2743    0.2488], 'String', 'Max Plot Frequency (Hz):', 'Visible', 'on');
        
        Data.handles.MaxFreqToPlotEdit =  uicontrol('Style', 'edit', 'Parent', Data.handles.FiltButtonPanel, 'Units', 'normalized', ...
            'Position', [0.3250    0.1191    0.1800    0.1700], 'String', num2str(22), 'BackgroundColor', [1 1 1], 'Visible', 'on');
        
        
        
        % Close out Init
        
        guidata(GUIFig, Data);
        
        function AddFilterPush(varargin)
            
            Data = guidata(GUIFig);
            
            whichType = get(Data.handles.FilterDropDown, 'Value');
            
            switch whichType
                
                case 1 % High-pass filter
                                        
                    fNorm = max([0 str2double(get(Data.handles.HighPassEdit, 'String'))/(Data.FrameRate/2)]);
                    
                    
                    
                    if (fNorm > 0) && (fNorm < 1)
                        
                        [bB, aB] = butter(10, fNorm, 'high'); 

                        Data.filters.LowPass{numel(Data.filters.LowPass)+1} = {bB, aB};

                        tabData = get(Data.handles.FiltTable, 'Data');
                        tabData(cellfun(@isempty, tabData)) = [];
                        
                        addString = sprintf('High-pass @ %s Hz', get(Data.handles.HighPassEdit, 'String'));
                        
                        if any(strcmp(addString, tabData))
                            msgbox('Identical filter already specified.', 'Filter Error!');
                        else
                        
                            tabData{length(tabData) + 1, 1} = addString;
                            set(Data.handles.FiltTable, 'Data', tabData);
                        
                        end
                    else
                        
                        msgbox('Filter cut-on must be greater than 0 and less than frame rate.', 'Filter Error!');
                    end
                    
                case 2 % Notch filter
                    

                    NotchFilt = str2double(get(Data.handles.NotchEditCenter, 'String'));
                    Bandwidth = str2double(get(Data.handles.NotchEditWidth, 'String'));
                    
                    NotchFiltWidth = (NotchFilt/((Data.FrameRate/2)*Bandwidth));
                    
                    if (NotchFilt > 0) && (NotchFilt < Data.FrameRate/2) && (NotchFiltWidth > 0)
                    
                        Wo = NotchFilt/(Data.FrameRate/2);  
                        BW = Wo/NotchFiltWidth;
                        [b1,a1] = iirnotch(Wo,BW); 

                        Data.filters.Notch{numel(Data.filters.Notch)+1} = {b1, a1};
                        
                        addString = sprintf('Notch @ %s / %.3d Hz', get(Data.handles.NotchEditCenter, 'String'), Bandwidth);
                        
                        tabData = get(Data.handles.FiltTable, 'Data');
                        tabData(cellfun(@isempty, tabData)) = [];
                        
                        if any(strcmp(addString, tabData))
                            msgbox('Identical filter already specified.', 'Filter Error!');
                        else
                        
                            tabData{length(tabData) + 1, 1} = addString;
                            set(Data.handles.FiltTable, 'Data', tabData);
                        
                        end
                        
                    else
                        msgbox(sprintf('Filter center must be greater than 0 and less than (frame rate)/2. \nFilter width must be greater than 0.'), 'Filter Error!');
                    end
                    
                case 3 % Comb filter

                    CombCenter = str2double(get(Data.handles.CombTextEdit, 'String'));
                    Bandwidth = str2double(get(Data.handles.CombEditWidth, 'String'));

                    CombWidth = CombCenter*(Bandwidth/(Data.FrameRate/2));
                    
                    if (CombCenter > 0) && (CombCenter < Data.FrameRate) && (CombWidth > 0)
  
                        [bC,aC] = iircomb(CombCenter, CombWidth, 'notch'); % Comb filter a 4th harmonic of Frame Rate 

                        Data.filters.Comb{numel(Data.filters.Comb)+1} = {bC, aC};
                        
                        addString = sprintf('Comb @ (F / %d) / %.3d Hz', CombCenter, Bandwidth);
                        
                        tabData = get(Data.handles.FiltTable, 'Data');
                        tabData(cellfun(@isempty, tabData)) = [];
                        
                        if any(strcmp(addString, tabData))
                            msgbox('Identical filter already specified.', 'Filter Error!');
                        else
                        
                            tabData{length(tabData) + 1, 1} = addString;
                            set(Data.handles.FiltTable, 'Data', tabData);
                        
                        end
                        
                    else
                        msgbox(sprintf('Filter harmonic must be greater than 0 and less than frame rate. \nFilter width must be greater than 0.'), 'Filter Error!');
                    end
                    
            end
            
            Data.filters.TableData = tabData;
            
            guidata(GUIFig, Data)
            
        end
        
        %%%
        
        function DeleteFilterPush(varargin)
            
            Data = guidata(GUIFig);
            whichFilter = get(Data.handles.FiltTable, 'UserData');
            whichFilter = whichFilter(1);
            
            dt = get(Data.handles.FiltTable, 'Data');
            
            % Determine filter type here
            FilterIs = find([~isempty(strfind(dt{whichFilter}, 'High-pass')), ...
                ~isempty(strfind(dt{whichFilter}, 'Notch')), ~isempty(strfind(dt{whichFilter}, 'Comb'))]);
            
            FilterTypes = {'High-pass', 'Notch', 'Comb'};
            
            SameTypeFilters = find(~cell2mat(cellfun(@isempty, strfind(dt, FilterTypes{FilterIs}), 'UniformOutput', false)'));
            
            % Clear out that filter
            
            switch FilterIs
                
                case 1 % High-pass filter
                    
                    Data.filters.LowPass(SameTypeFilters == whichFilter) = [];
                    
                case 2 % Notch filter
                    
                    Data.filters.Notch(SameTypeFilters == whichFilter) = [];
                    
                case 3 % Comb filter
                    
                    Data.filters.Comb(SameTypeFilters == whichFilter) = [];
                    
            end
            
            Data.filters.TableData(whichFilter) = [];
                        
            % Redraw table without deleted filter
            
            delete(Data.handles.FiltTable)
            Data.handles.FiltTable = uitable('Parent', Data.handles.FiltListPanel, 'Units', 'normalized', ...
                'Position', [.05 .05 .9 .9], 'Data', Data.filters.TableData, 'ColumnName', 'Filter Name');
        
            TableSize = getpixelposition(Data.handles.FiltTable);
        
            set(Data.handles.FiltTable, 'ColumnWidth', {TableSize(3)*.88}, 'CellSelectionCallback', @(src,evnt)set(src,'UserData',evnt.Indices))
            
            guidata(GUIFig, Data);
            
        end
        
        %%%
        
        function ChangeFilterType(varargin)
            
            whichType = get(varargin{1}, 'Value');
            
            switch whichType
                
                case 1 
                    set([Data.handles.HighPassText, Data.handles.HighPassEdit], 'Visible', 'on');
                    set([Data.handles.NotchTextCenter, Data.handles.NotchEditCenter, ...
                        Data.handles.NotchTextWidth, Data.handles.NotchEditWidth, ...
                        Data.handles.CombTextCenter, Data.handles.CombTextEdit, ...
                        Data.handles.CombTextWidth, Data.handles.CombEditWidth], 'Visible', 'off');
                    
                case 2 
                    set([Data.handles.NotchTextCenter, Data.handles.NotchEditCenter, ...
                        Data.handles.NotchTextWidth, Data.handles.NotchEditWidth], 'Visible', 'on');
                    set([Data.handles.HighPassText, Data.handles.HighPassEdit, ...
                        Data.handles.CombTextCenter, Data.handles.CombTextEdit, ...
                        Data.handles.CombTextWidth, Data.handles.CombEditWidth], 'Visible', 'off');
                    
                case 3
                    set([Data.handles.CombTextCenter, Data.handles.CombTextEdit, ...
                        Data.handles.CombTextWidth, Data.handles.CombEditWidth], 'Visible', 'on');
                    set([Data.handles.HighPassText, Data.handles.HighPassEdit, ...
                        Data.handles.NotchTextCenter, Data.handles.NotchEditCenter, ...
                        Data.handles.NotchTextWidth, Data.handles.NotchEditWidth], 'Visible', 'off');
            end
            
        end
        
        
    end

    %%%

    function CloseFiltWindow(varargin)
        
        close(Data.handles.FilterFig);
        
    end

    %%%
    
    function DeleteFilterWindow(varargin)
        
        % Save filter specs to use again
%         disp('Save and close')
        
        Data.filters.TableData = get(Data.handles.FiltTable, 'Data');
        Data.filters.MaxFreqToPlot = str2double(get(Data.handles.MaxFreqToPlotEdit, 'String'));
        guidata(GUIFig, Data);
        
    end



%%%%%

    function SelectPtsFcn(varargin)
        
        Data = guidata(GUIFig);
        
        if isfield(Data.handles, 'SelectPoint') 
            if isobject(Data.handles.SelectPoint)
                
               % disp('object exists')
    
            else
                
            Data.handles.SelectPoint = impoint(Data.handles.ImageAxes);
            ChangePointPosition;

            fcn = makeConstrainToRectFcn('impoint',get(gca,'XLim'),get(gca,'YLim'));
            setPositionConstraintFcn(Data.handles.SelectPoint,fcn);
            
            addNewPositionCallback(Data.handles.SelectPoint, @ChangePointPosition);
            
            end
        end

        
        function ChangePointPosition(varargin)
            
            postHere = (round(Data.handles.SelectPoint.getPosition()));
            
            setString(Data.handles.SelectPoint, sprintf('(%d, %d)', postHere(1), postHere(2)));
            
        end
        
        % Change 'Select Point' button to allow for calc, display of trace
        % for that pixel
        
        set(Data.handles.SelectPts, 'String', 'Show Point Results', 'Callback', @ShowTrace);

        guidata(GUIFig, Data);
        
    end

%%%%%

    function ShowTrace(varargin)
        
        Data = guidata(GUIFig);
       
        whichPixel = (round(Data.handles.SelectPoint.getPosition()));
        
        VidFrames = squeeze(Data.VidFrames(whichPixel(2), whichPixel(1), :));
        
        % Filter trace here

        filtvectRaw = (VidFrames - mean(VidFrames))';
        
        zeroPadLength = 2^(1+nextpow2(Data.NFrames));
        
        % Take filtvect and resample it to have a constant time axis
        filtvect = (interp1(Data.metaData.RelTimestamp, filtvectRaw, Data.TimeVector));
        filtvect(end) = [];
        
        if (numel(Data.filters.LowPass) + numel(Data.filters.Notch) + numel(Data.filters.Comb)) > 0
            
            for f = 1:numel(Data.filters.LowPass)
                filtvect = filtfilt(Data.filters.LowPass{f}{1}, Data.filters.LowPass{f}{2}, filtvect); % Low pass
%                 filtvect = filter(Data.filters.LowPass{f}{1}, Data.filters.LowPass{f}{2}, filtvect); % Low pass
            end
            
            for f = 1:numel(Data.filters.Notch)
                filtvect = filtfilt(Data.filters.Notch{f}{1}, Data.filters.Notch{f}{2}, filtvect); % Notch filters
            end
            
            for f = 1:numel(Data.filters.Comb)
                filtvect = filtfilt(Data.filters.Comb{f}{1}, Data.filters.Comb{f}{2}, filtvect); % Comb filters
            end
            
        end
        
%         gpuArray(filtVect);
        
        outFFT = (fft(filtvect, zeroPadLength))/(Data.NFrames-1);
%         outVar = var(filtvect);
        outHalf = 2*abs(outFFT(1:(zeroPadLength/2 + 1)));
         FFTImg = Data.TimeDomain(find(outHalf == max(outHalf), 1, 'first'));
        
        
       
        
        % Display trace results
        
        Data.handles.TraceFigs = figure();
        
        GUIPosition = get(Data.handles.GUIFig, 'Position');
        set(Data.handles.TraceFigs, 'Position', [max([GUIPosition(1)+(GUIPosition(3)/2)-(950/2), 0]), ...
            max([GUIPosition(2)+(GUIPosition(4)/2)-(580/2)-100 0]), 950, 580], 'Toolbar', 'figure', ...
            'Name', 'Single Point Results', 'Numbertitle', 'off')
        
        Data.handles.TraceAxis(1) = axes('Parent', Data.handles.TraceFigs, ...
            'Units', 'normalized', 'Position', [.066 .63 .915 .35]);

        Data.handles.TraceAxis(2) = axes('Parent', Data.handles.TraceFigs, ...
            'Units', 'normalized', 'Position', [.066 .077 .51 .48]);
        
        plot(Data.handles.TraceAxis(1), Data.metaData.RelTimestamp/1000, VidFrames, 'k')
        set(Data.handles.TraceAxis(1), 'NextPlot', 'add')
        plot(Data.handles.TraceAxis(1), Data.TimeVector(1:(end-1))/1000, filtvect, 'r')
        
        xlabel(Data.handles.TraceAxis(1), 'Time (sec)');
        ylabel(Data.handles.TraceAxis(1), 'Amplitude (a.u.)');
        
        
        set(Data.handles.TraceAxis(1), 'XLim', [0, max([Data.TimeVector(end-1)/1000, Data.metaData.RelTimestamp(end)/1000])]);
        
        plot(Data.handles.TraceAxis(2), Data.TimeDomain, outHalf, 'k')
        set(Data.handles.TraceAxis(2), 'XLim', [0, max(Data.TimeDomain)]);
        xlabel(Data.handles.TraceAxis(2), 'Frequency (Hz)');
        ylabel(Data.handles.TraceAxis(2), 'Power');
        set(Data.handles.TraceAxis(2), 'NextPlot', 'add');
        plot(Data.handles.TraceAxis(2), FFTImg, max(outHalf(:)), 'ro', 'MarkerSize', 10, 'LineWidth', 2);
        
        
        Data.handles.TextAxis = axes('Parent', Data.handles.TraceFigs, ...
            'Units', 'normalized', 'Position', [.6 0 .4 .555], 'Visible', 'off');
        outlineRectangle = plot(Data.handles.TextAxis, [0 .95 .95 0 0], [.05 .05 1 1 .05], 'k');
        axis(Data.handles.TextAxis, [0 1 0 1]);
        set(Data.handles.TextAxis, 'Visible', 'off')
        text(.05, .9, sprintf('File Name: %s', get(Data.handles.LoadText, 'String')), 'Interpreter', 'none', 'FontSize', 12);
        text(.05, .8, sprintf('Position: ( %d, %d )', (round(Data.handles.SelectPoint.getPosition()))), 'Interpreter', 'none', 'FontSize', 12);
        text(.05, .7, sprintf('Peak Frequency: %.2f Hz', FFTImg), 'Interpreter', 'none', 'FontSize', 12);
        text(.05, .6, sprintf('Peak Power: %.2f', max(outHalf(:))), 'Interpreter', 'none', 'FontSize', 12);
        text(0.05, .5, 'Filters:', 'FontSize', 12);
        text(.1, .42, Data.filters.TableData, 'Interpreter', 'none', 'FontSize', 10);

        % Buttons
        
        Data.handles.ClosePointTrace = uicontrol('Parent', Data.handles.TraceFigs, ...
            'Style', 'pushbutton', 'Units', 'normalized', ...
            'Position', [.88 .04 .08 .06], 'FontSize', 12,...
            'String', 'Close', 'Callback', @CloseTraceButton);
        
        function CloseTraceButton(varargin)
            
          close(get(varargin{1}, 'Parent'))
            
            
        end
        
        
%         figure()
%         plot(Data.metaData.RelTimestamp, VidFrames)
%         title('Original Trace')
%         
%         figure()
%         plot(Data.TimeVector(1:(end-1))/1000, filtvect)
%         title('Filtered Trace')
%         
%         figure()
%         plot(Data.TimeDomain, outHalf)
%         title('Filtered FFT')
%         
        
    end



%%%%%

    function DisplayImageInAxes(varargin)
        
        Data = guidata(findobj('Tag', 'GUIFig'));
        
        WhichImage = get(Data.handles.AxisSlider, 'Value');
        
        % When 'NextPlot' is set to 'replace', new plot erases
        % ButtonDownFcn (along with other settings).  
        % Need to set 'NextPlot' to 'add', clear out old one, and put a new
        % image in the back (if there are clicked points in this frame).
        
        set(Data.handles.ImageAxes, 'NextPlot', 'add')
        delete(findobj('Parent', Data.handles.ImageAxes, 'Type', 'image'));
        
        
        imagesc(Data.VidFrames(:,:,WhichImage), 'Parent', Data.handles.ImageAxes);
        set(Data.handles.ImageAxes, 'XTick', [], 'YTick', []);
        axis image
        
%         if isfield(Data.handles, 'SelectPoint')
%             if Data.handles.SelectPoint.isvalid()
%                 uistack(Data.handles.SelectPoint, 'top')
%             end
%         end
        
        uistack(findobj('Parent', Data.handles.ImageAxes, 'Type', 'image'), 'bottom')
        
        set(findobj('Parent', Data.handles.ImageAxes, 'Type', 'image'), 'HitTest', 'off');
                
    end


%%%%%

    function PlotPtsFcn(varargin)
        
        Data = guidata(GUIFig);
        
        OnButtons = findobj('Parent', Data.handles.ButtonPanel, 'Enable', 'on', 'Style', 'pushbutton');
        set(OnButtons, 'Enable', 'off')
        set(Data.handles.GUIFig, 'pointer', 'watch');
        drawnow;
        
        FFTImg = zeros(Data.ImgMetaData.height*Data.ImgMetaData.width, 1);
        FFTAmp = FFTImg;
%         VarImg = FFTImg;
               
        zeroPadLength = 2^(1+nextpow2(Data.NFrames));

        % Go ahead and do this for the entire matrix at once.  Hopefully is
        % faster than doing it in pieces later.  ~4.3 seconds for whole
        % stack
        Dt = reshape(Data.VidFrames, (size(Data.VidFrames, 1)*size(Data.VidFrames, 2)), size(Data.VidFrames, 3));
        Dt = detrend(Dt, 'constant');
%         Dt = interp1qr(Data.metaData.RelTimestamp, Dt', Data.TimeVector);
        
        %%%%%%%%%
        % interp1qr code recalcs a lot of pieces that only need to be done
        % once.  Here we can calculate the xi_pos vector one time since all
        % pixels need to get interpolated to the same time axis.  

        % For each 'xi', get the position of the 'x' element bounding it on the left [p x 1]
        [~,xi_pos] = histc(Data.TimeVector, Data.metaData.RelTimestamp);
        xi_pos = max(xi_pos, 1);     % To avoid index=0 when xi < x(1)
        xi_pos = min(xi_pos, size(Data.metaData.RelTimestamp, 1)-1);   % To avoid index=m+1 when xi > x(end).

        % 't' matrix [p x 1]
        dxi = Data.metaData.RelTimestamp-Data.TimeVector(xi_pos);
        dx = Data.TimeVector(xi_pos+1)-Data.TimeVector(xi_pos);
        t = dxi./dx;

        % End interp1qr code.  yi calculate on a per-pixel basis in loop
        % below.  Total time cut down by ~40%.  Hair under 2 min for an
        % entire data set.  
        %%%%%%%%%%%%%%%%%%%%%%        
        
        
        w = waitbar(0, 'Processing stack...');
tic        
        for pix = 1:numel(FFTImg)

%       for pix = 1:10000;
            if mod(pix, 1000) == 0
                waitbar(pix/numel(FFTImg), w);
            end

            
%             [p1, p2] = ind2sub([Data.ImgMetaData.height, Data.ImgMetaData.width], pix);
        
            % Filter trace here

%             filtvectRaw = squeeze((Data.VidFrames(p1, p2, :) - mean(Data.VidFrames(p1, p2, :))))';
            filtvectRaw = Dt(pix, :)';

            % Take filtvect and resample it to have a constant time axis
            filtvect = filtvectRaw(xi_pos,:) + t(:, 1).*(filtvectRaw(xi_pos+1,:)-filtvectRaw(xi_pos,:));
            filtvect(Data.TimeVector<Data.metaData.RelTimestamp(1) | Data.TimeVector>Data.metaData.RelTimestamp(end),:) = NaN;
            filtvect(end) = [];
            
            

            if (numel(Data.filters.LowPass) + numel(Data.filters.Notch) + numel(Data.filters.Comb)) > 0

                for f = 1:numel(Data.filters.LowPass)
                    filtvect = filtfilt(Data.filters.LowPass{f}{1}, Data.filters.LowPass{f}{2}, filtvect); % Low pass
%                     filtvect = fftfilt(Data.filters.LowPass{f}{1}, Data.filters.LowPass{f}{2}, filtvect); % Low pass
                end

                for f = 1:numel(Data.filters.Notch)
                    filtvect = filtfilt(Data.filters.Notch{f}{1}, Data.filters.Notch{f}{2}, filtvect); % Notch filters
%                     filtvect = filter(Data.filters.Notch{f}{1}, Data.filters.Notch{f}{2}, filtvect); % Notch filters
                end

                for f = 1:numel(Data.filters.Comb)
                    filtvect = filtfilt(Data.filters.Comb{f}{1}, Data.filters.Comb{f}{2}, filtvect); % Notch filters
%                     filtvect = filter(Data.filters.Comb{f}{1}, Data.filters.Comb{f}{2}, filtvect); % Notch filters
                end

            end

                outFFT = (fft(filtvect, zeroPadLength))/(Data.NFrames-1);
%                 outVar = var(filtvect);
                outHalf = 2*abs(outFFT(1:(zeroPadLength/2 + 1)));

                FFTImg(pix) = Data.TimeDomain(find(outHalf == max(outHalf), 1, 'first'));
                FFTAmp(pix) = log(max(outHalf).^2);
%                 VarImg(pix, :) = outVar;

        end
        
        FFTImg = reshape(FFTImg, size(Data.VidFrames, 1), size(Data.VidFrames, 2));
        FFTAmp = reshape(FFTAmp, size(Data.VidFrames, 1), size(Data.VidFrames, 2));
        
        toc
        


        waitbar(1, w, 'Plotting...');
        
                    %%%%%%%%%%%
            % Plotting

            % [FFTscaleHist, FFTscaleBins] = hist(FFTAmp(:), round(sqrt(numel(FFTAmp))));
            % FFTthresh = FFTscaleBins(FFTscaleHist == max(FFTscaleHist(:)));
            % FFTscale = max(FFTImg(FFTAmp > 20*FFTthresh));

            %[VarScaleHist, VarScaleBins] = hist(VarImg(:), round(sqrt(numel(VarImg))));
            %VarThresh = VarScaleBins(VarScaleHist == max(VarScaleHist(:)));
%             VarThresh = 0.01*max(VarImg(:));
%             VarScale = max(FFTImg(VarImg > VarThresh));
% 
%             HSVit = ones(size(FFTImg, 1), size(FFTImg, 2), 3);
% 
%             HSVit(:,:,1) = FFTImg/max(FFTImg(:));
%             %HSVit(:,:,2) = ones(size(FFTImg));
%             %HSVit(:,:,3) = VidSum/max(VidSum(:));
%             HSVit(:,:,3) = FFTAmp/max(FFTAmp(:));
            
%             Vsqrt = log(VarImg);
%             Feamp = exp(FFTAmp);

            FreqCutoff = Data.filters.MaxFreqToPlot;
            ColorMapIt = Vector2Colormap_setscale((FFTImg), 'jet', [0 FreqCutoff]).*Vector2Colormap_setscale((FFTAmp)/max(FFTAmp(:)), 'gray', [0 1]);
            %ColorMapIt = ColorMapIt.*repmat(sqrt(VarImg)/max(sqrt(VarImg(:))), [1 1 3]);
% 


%             FFTrgb = hsv2rgb(HSVit);
%             FFTrgb(FFTrgb < 0) = 0;
% 
%             figure(12)
%             image(FFTrgb)
%             title('FFTrgb Max Time Domain + Amp');
%             c = colorbar;
%             ylabel(c, 'Frequency (Hz)');

            dispFig = figure();
            set(dispFig, 'Toolbar', 'figure', 'Menubar', 'none');
            image(ColorMapIt)
            set(gca, 'XTick', []);
            set(gca, 'YTick', []);
            axis image
            title('Color - Frequency; Intensity - Amplitude', 'FontSize', 12);
            c = colorbar;
            ylabel(c, 'Frequency (Hz)', 'FontSize', 12);
            set(c, 'YTick', 0:8:64);
            set(c, 'YTickLabel', num2str(((0:8:64)*(FreqCutoff/64))'));
            set(c, 'FontSize', 12);


%             figure(14)
%             imagesc(FFTImg)
%             title('FFT peak in time domain');
            
            set(OnButtons, 'Enable', 'on')
            set(Data.handles.GUIFig, 'pointer', 'arrow');
            drawnow;
            
            delete(w)
            
            FilterTable = Data.filters.TableData;
            
            
            saveName = sprintf('%s_%s.mat', 'FFT_Results', strtok(get(Data.handles.LoadText, 'String'), '.'));
            
            
            save(fullfile(get(Data.handles.OutputText, 'String'), saveName), 'FFTImg', 'FFTAmp', 'FreqCutoff', 'FilterTable');
        
        
    end


end


