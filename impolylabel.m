classdef impolylabel < impoly
   
    % Found : http://www.freag.net/en/t/3xvt6/imellipse_equiv, and slightly
    % modified

    properties
        Handle
        ParentPosition
    end
        
    methods 
        %Constructor
        function obj = impolylabel(varargin)
            obj = obj@impoly(varargin{:});
            
            obj.ParentPosition = getpixelposition(get(varargin{1}, 'Parent'));
        end %impolylabel
        
        function setParentPosition(obj, arg)
            obj.ParentPosition = getpixelposition(get(arg, 'Parent'));
        end

        function setHandle(obj, val)
            obj.Handle = val; 
        end
        
            function setString(obj,str,textOp)
                if nargin < 3
                    textOp = struct([]);
                end %if
                %check to see if there is already a text label
                existLabel = findobj(obj.h_group,'Type','text','Tag','label');

                if isempty(existLabel)
                    existLabel = text('Parent',obj.h_group,...
                        'Position',obj.getTextPos,...
                        'String',str,...
                        'HorizontalAlignment','left',...
                        'BackgroundColor','w',...
                        'FontSize', 8, ...
                        'Tag','label');

                    obj.addNewPositionCallback(@obj.textPosition_Callback);
                    
                    textSize = get(existLabel, 'Extent');
                    pos = obj.getPosition;
                    [maxSum,maxIdx] = max(sum(pos,2)); %#ok if you have up to date version use ~
                    textPos = pos(maxIdx,:) + 8;
                    
                    if xor((sum([textPos(1), textSize(3)]) >= obj.ParentPosition(3)), (sum([textPos(2), textSize(4)]) >= obj.ParentPosition(4)))
                        if sum(textSize([1 3])) >= obj.ParentPosition(3) && not(sum(textSize([2 4])) >= obj.ParentPosition(4))
                            set(existLabel(1), 'Position', obj.outRightTextBox(textSize(3)));
                        elseif sum(textSize([2 4])) >= obj.ParentPosition(4) && not(sum(textSize([1 3])) >= obj.ParentPosition(3))
                            set(existLabel(1), 'Position', obj.outTopTextBox(textSize(4)));
                        end
                    elseif and((sum(textSize([1 3])) >= obj.ParentPosition(3)), (sum(textSize([2 4])) >= obj.ParentPosition(4)))
                        set(existLabel(1), 'Position', obj.outBothTextBox(textSize(3)));
                    end
                else
                    %update the existing tag
                    set(existLabel(1),'String',str);
                    existLabel = findobj(obj.h_group,'Type','text','Tag','label');
                    textSize = get(existLabel, 'Extent');
                    pos = obj.getPosition;
                    [maxSum,maxIdx] = max(sum(pos,2)); %#ok if you have up to date version use ~
                    textPos = pos(maxIdx,:) + 8;
                    
                    if xor((sum([textPos(1), textSize(3)]) >= obj.ParentPosition(3)), (sum([textPos(2), textSize(4)]) >= obj.ParentPosition(4)))
                        if sum(textSize([1 3])) >= obj.ParentPosition(3) && not(sum(textSize([2 4])) >= obj.ParentPosition(4))
                            set(existLabel(1), 'Position', obj.outRightTextBox(textSize(3)));
                        elseif sum(textSize([2 4])) >= obj.ParentPosition(4) && not(sum(textSize([1 3])) >= obj.ParentPosition(3))
                            set(existLabel(1), 'Position', obj.outTopTextBox(textSize(4)));
                        end
                    elseif and((sum(textSize([1 3])) >= obj.ParentPosition(3)), (sum(textSize([2 4])) >= obj.ParentPosition(4)))
                        set(existLabel(1), 'Position', obj.outBothTextBox(textSize(3)));
                    end
                    
                end %if

                if ~isempty(textOp)
                    set(existLabel(1),textOp);
                    uistack(obj.Handle, 'top');
                    obj.setColor([.9 .5 0])
                end %if


            end %setString
        
        
    end %methods
    
    methods (Access = private)
        
        function textPosition_Callback(obj,varargin)
            existLabel = findobj(obj.h_group,'Type','text','Tag','label');
            if ~isempty(existLabel)
                set(existLabel(1),'Position',obj.getTextPos());
            end %if

        end %setPosition
        
        function textPos = getTextPos(obj)
            pos = obj.getPosition;
            [maxSum,maxIdx] = max(sum(pos,2)); %#ok if you have up to date version use ~
            textPos = pos(maxIdx,:) + 8;
        end %getTextPos
        
        function newTextPos = outRightTextBox(obj, varargin)
            pos = obj.getPosition;
%             obj.setColor([0 1 0]);
            [maxSum,maxIdx] = min((pos(:,1))); %#ok if you have up to date version use ~
            newTextPos = pos(maxIdx,:) - [varargin{1} 0] - 8;
        end
        
        function newTextPos = outTopTextBox(obj, varargin)
            pos = obj.getPosition;
%             obj.setColor([1 0 0]);
            [minSum,minIdx] = min(sum(pos,2)); %#ok if you have up to date version use ~
            newTextPos = pos(minIdx,:) - [2*varargin{1}/3, 2*varargin{1}/3];
        end
        
        function newTextPos = outBothTextBox(obj, varargin)
            pos = obj.getPosition;
%             obj.setColor([0 0 1]);
            [minSum,minIdx] = min(sum(pos,2)); %#ok if you have up to date version use ~
            newTextPos = pos(minIdx,:) - [2*varargin{1}/3, 2*varargin{1}/3];
        end
        
        
        
    end %private methos
end %impoly

